<?php

namespace Shahnewaz\IntelleAppUpdater\Providers;

use Illuminate\Support\ServiceProvider;
use Shahnewaz\IntelleAppUpdater\Services\IntelleAppUpdaterService;

class IntelleAppUpdaterServiceProvider extends ServiceProvider
{
    /** 
    * This provider cannot be deferred since it loads routes.
    * If deferred, run `php artisan route:cache`
    **/
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot () {
        $this->load();
        $this->publish();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register () {
        $this->mergeConfigFrom($this->packagePath('config/intelle_app_updater.php'), 'intelle_app_updater');
        // Register Permissible Service
        $this->app->singleton('intelle_app_updater', function ($app) {
            return new IntelleAppUpdaterService;
        });
    }

    // Root path for package files
    private function packagePath ($path) {
        return __DIR__."/../../$path";
    }

    // Facade provider
    public function provides () {
        return ['intelle_app_updater'];
    }

    // Class loaders for package
    public function load () {
        // Routes
        $this->loadRoutesFrom($this->packagePath('src/routes/web.php'));
        // Migrations
        $this->loadMigrationsFrom($this->packagePath('database/migrations'));
        // Translations
        $this->loadTranslationsFrom($this->packagePath('resources/lang'), 'intelle_app_updater');
         // Views
        $this->loadViewsFrom($this->packagePath('resources/views'), 'intelle_app_updater');
    }

    // Publish required resouces from package
    private function publish () {
        // Publish Translations
        $this->publishes([
            $this->packagePath('resources/lang') => resource_path('lang/vendor/intelle_app_updater'),
        ]);
        
        // Publish Permissible Config
        $this->publishes([
            $this->packagePath('config/intelle_app_updater.php') => config_path('intelle_app_updater.php'),
        ], 'config');

        // Publish views
        $this->publishes([
            $this->packagePath('resources/views') => resource_path('views/vendor'),
        ], 'views');
    }
}
