<?php

namespace Shahnewaz\IntelleAppUpdater\Http\Controllers;

use Artisan;
use Storage;
use ZipArchive;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;
use App\Http\Controllers\Controller;

class UpdateController extends Controller
{

  public $server;
  public $client;
  public $appName;
  public $version;
  /**
  * The filesystem instance.
  *
  * @var Filesystem
  */
  protected $files;

  public function __construct () {
    $this->client = new Client();
    $this->server = config('intelle_app_updater.server', 'updater.intelle-hub.com');
    $this->appName = config('intelle_app_updater.app', 'redprint');
    $this->files = new Filesystem;
    $this->version = getIntelleAppVersion();
  }

  public function index () {
    $version = getIntelleAppVersion();
    return view('intelle_app_updater::intelle-app-updater.index')->with('version', $this->version);
  }

  public function getUpdateData () {
    $response = $this->client->request('GET', $this->server.'/api/v1/'.$this->appName.'/check-update/'.$this->version)->getBody();
    $response = json_decode($response, true);
    return $response;
  }

  public function checkUpdate () {
    $response = $this->getUpdateData();
    return response()->json($response);
  }

  public function getUpdate () {
    $response = $this->getUpdateData();
    $response['download_url'] = $this->server.$response['update']['download_url'];
    return response()->json(['data' => $response]);
  }

  public function applyUpdate () {
    $log = [];
    $result = ['message' => 'Update applied!', 'log' => $log, 'version' => $this->version];

    $response = $this->getUpdateData();
    $updateZip = $this->server.$response['update']['download_url'];
    // Download zip locally
    $destination = 'updates/'.$this->version.'/update.zip';
    Storage::disk('storage')->put($destination, file_get_contents($updateZip));
    $files = $response['update']['files'];

    // Extract Zip
    $zip = new ZipArchive;

    if ($zip->open(storage_path($destination)) === TRUE) {
      try {
        $zip->extractTo(base_path());
        $zip->close();
        $migrate = false;
        if ($response['update']['migration'] == 1) {
          $migrate = true;
        }
        $this->cleanup($migrate);
        $result['version'] = getIntelleAppVersion();
        $result['message'] = 'Software updated to '.$result['version'].' successfully!';
      } catch (\Exception $e) {
        $result['message'] = 'Could not write files. Make sure the app is in developer mode and you\'re not running the update in production.';
      }
    } else {
        $result['message'] = 'Update file corrupted or not accessible. Please try again later. Contact Redprint team if the problem persists.';
    }

    return response()->json(['data' => $result]);
  }

  public function cleanup ($migrate = false) {
    // Dump Composer autoload
    try {
        $process = system('composer dump-autoload');
    } catch (\Exception $e) {
        // Silence!
    }
    if ($migrate) {
      Artisan::call('migrate');
    }
    Artisan::call('clear-compiled');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return true;
  }


}
