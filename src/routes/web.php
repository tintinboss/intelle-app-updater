<?php

Route::namespace('Shahnewaz\IntelleAppUpdater\Http\Controllers')->middleware(['web', 'auth'])->group(function () {
    $app = config();
    Route::get('/updater', 'UpdateController@index')->name('intelle-app-updater.index');
    Route::get('/updater/check', 'UpdateController@checkUpdate')->name('intelle-app-updater.check');
    Route::get('/updater/download/zip', 'UpdateController@getUpdate')->name('intelle-app-updater.download');
    Route::post('/updater/apply', 'UpdateController@applyUpdate')->name('intelle-app-updater.apply');
});
