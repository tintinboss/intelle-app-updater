<?php

  function getIntelleAppVersion () {
    $app = config('intelle_app_updater.app', '');
    $appType = config('intelle_app_updater.type', 'package') === 'package' ? 'vendor' : '';
    $vendor = config('intelle_app_updater.vendor', '');
    $versionFilePath = base_path($appType.'/'.$vendor.'/'.$app.'/version.json');
    $version = json_decode(file_get_contents($versionFilePath));
    return $version->current;
  }

  function intelleUpdaterMenu () {
    $menu = include(__DIR__.'/../../config/menu.php');
    return $menu;
  }
