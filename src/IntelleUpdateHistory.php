<?php 
namespace Shahnewaz\IntelleAppUpdater;

use Illuminate\Database\Eloquent\Model;

class IntelleUpdateHistory extends Model
{
    protected $table = 'intelle_update_history';
}
