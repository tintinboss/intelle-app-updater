@extends('redprintUnity::page')

@section('title') Updater @stop

@section('page_title') Redprint Updater @stop
@section('page_subtitle') Update Redprint to latest version @stop
@section('page_icon') <i class="icon-download"></i> @stop

@section('content')
    <div class="card" id="app">
        <div class="card-body">
            <p>Current App Version: @{{ version }}</p>
            <a href="#" @click.prevent="checkUpdate" class="btn btn-info btn-md"><i class="icon-checkmark"></i>&nbsp;&nbsp;Check for Update</a>  
            <div id="updateChecker">
              <p id="updateInformation"></p>
            </div>

            <div v-if="updatable">
                <hr />
                <a href="#" @click.prevent="downloadUpdate" class="btn btn-info btn-md"><i class="icon-download"></i>&nbsp;&nbsp;Download Update</a>
                <a href="#" @click.prevent="applyUpdate" class="btn btn-primary btn-md"><i class="icon-cloud-download"></i>&nbsp;&nbsp;Apply Update</a>
            </div>

            <hr />
            <div class="alert alert-info" role="alert" v-if="message">
              @{{ message }}
            </div>

            <div class="alert alert-info" role="alert" v-if="error">
              @{{ error }}
            </div>

        </div>
    </div>
@stop



@section('head-js')
    @parent
    <script src="/vendor/redprint/vendor/vue/vue.min.js"></script>
    <script src="/vendor/redprint/vendor/axios/axios.min.js"></script>
@stop

@section('post-js')
@parent
    <script>
        var token = document.head.querySelector('meta[name="csrf-token"]');
        axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
        axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

        var app = new Vue({
            el: '#app',
            data: {
                submitted: false,
                success: false,
                error: false,
                version: {!! json_encode($version) !!},
                updatable: false,
                message: '',
                error: '',
                errors: {}
            },
            computed: {
            },
            mounted: function () {
            },
            methods:{
                checkUpdate: function () {
                    var self = this

                    var route = {!! json_encode(route('intelle-app-updater.check')) !!}
                    axios.get(route)
                      .then(function (response) {
                        console.log(JSON.stringify(response))
                        self.updatable = response.data.update_available
                        self.message = response.data.message
                      })
                      .catch(function (response) {
                        console.log(JSON.stringify(response))
                        self.error = JSON.stringify(response.data)
                      });
                },
                downloadUpdate: function () {
                    var self = this
                    var route = {!! json_encode(route('intelle-app-updater.download')) !!}
                    axios.get(route)
                    .then(function (response) {
                        var download_url = response.data.data.download_url
                        console.log(download_url)
                        window.open(download_url)
                    })
                    .catch(function (response) {
                        console.log(JSON.stringify(response))
                        self.error = JSON.stringify(response.data)
                    });
                },
                applyUpdate: function () {
                    var self = this
                    var route = {!! json_encode(route('intelle-app-updater.apply')) !!}
                    axios.post(route)
                    .then(function (response) {
                        console.log(response.data)
                        self.message = response.data.data.message
                        self.version = response.data.data.version
                    })
                    .catch(function (response) {
                        console.log(JSON.stringify(response))
                        self.error = JSON.stringify(response.data)
                    });
                }
            }
        });

    </script>
@stop