<?php

return [
  'Updater',
  [
    'text'        => 'Updater',
    'route'       => 'intelle-app-updater.index',
    'icon'        => 'icon-download',
  ]
];